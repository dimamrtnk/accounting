﻿using Accounting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accounting.Controllers
{
    public class AuthController : Controller
    {
        AccountingContext context = new AccountingContext();
      
      
        public ActionResult Index()
        {
            if (HttpContext.Request.Cookies.AllKeys.Contains("isauth"))
            {
           
                if (MvcApplication.IsAuthorized())
                {
                    return RedirectPermanent("/Employees/Index");
                }
                else return View();
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult Login(string Login, string Password)
        {
            var users = context.Users;
            if (users.Where(x => x.Login == Login).Count() > 0)
            {
                if (users.First(x => x.Login == Login).Password == Password)
                {
                    HttpContext.Response.Cookies["user-login"].Value = Login;
                    HttpContext.Response.Cookies["isauth"].Value = "1";
                    return RedirectPermanent("/Employees/Index");
                }
                else
                {
                    ViewBag.ShowError = true;
                    return View("Index");
                }
            }
            else
            {
                ViewBag.ShowError = true;
                return View("Index");
            }

        }
        public ActionResult Logout()
        {
            HttpContext.Response.Cookies["user-login"].Value = "";
            HttpContext.Response.Cookies["isauth"].Value = "0";

            return Redirect("Index");
        }
        public ActionResult ForgotPassword()
        {
            return View();
        }
       
        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }
    }
}