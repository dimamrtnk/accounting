﻿using Accounting.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Accounting.Controllers
{
    public class CalcController : Controller
    {
        AccountingContext context = new AccountingContext();
        public ActionResult Index()
        {
            if (MvcApplication.IsAuthorized())
            {
                ViewBag.Employees = new SelectList(context.Employees.ToList(), "Id", "FullName");
                return View();
            }
            else return RedirectToAction("Index", "Auth");
        }
        [HttpPost]
        public ActionResult GetTable(string Id, string firstDate, string secondDate)
        {
            List<CalculationModel> list = GetList(Id, firstDate, secondDate);
            ViewBag.DataList = list;
            ViewBag.SalarySum = list.Sum(x => decimal.Parse(x.Salary)).ToString();
            ViewBag.TotalSum = list.Sum(x => decimal.Parse(x.Sum)).ToString();
            ViewBag.FirstSum = list.Sum(x => decimal.Parse(x.EuroSum)).ToString();

            decimal sum = 0;
            foreach (var item in list)
            {
                try
                {
                    sum += decimal.Parse(item.AdditionalBenefits);
                }
                catch
                {

                }
            }
            ViewBag.TotalBenefit = sum>0?Math.Round(sum, 2).ToString():"";

            ViewBag.Id = Id;
            ViewBag.firstDate = firstDate;
            ViewBag.secondDate = secondDate;
            return View();
        }
        public void SaveToExcel(string Id, string firstDate, string secondDate)
        {
            var table = new System.Data.DataTable("data");
            var list = GetList(Id, firstDate, secondDate);
            table.Columns.Add("Дата", typeof(string));
            table.Columns.Add("Курс  ЄВРО", typeof(string));
            table.Columns.Add("Ставка ЄВРО (константа)", typeof(string));
            table.Columns.Add("Всього нараховано, грн", typeof(string));
            table.Columns.Add("Курс", typeof(string));
            table.Columns.Add("Ставка", typeof(string));
            table.Columns.Add("Всього нараховано за відрядження, грн", typeof(string));
            table.Columns.Add("Додаткове благо", typeof(string));

            foreach (var item in list)
            {
                table.Rows.Add(item.Date, item.EuroRate, item.EuroSalary, item.EuroSum, item.Rate, item.Salary, item.Sum, item.AdditionalBenefits); 

            }
            decimal sum = 0;
            foreach (var item in list)
            {
                try
                {
                    sum += decimal.Parse(item.AdditionalBenefits);
                }
                catch
                {

                }
            }
            table.Rows.Add("Всього","","", list.Sum(x => decimal.Parse(x.EuroSum)).ToString(),"","", list.Sum(x => decimal.Parse(x.Sum)).ToString(), sum > 0 ? Math.Round(sum, 2).ToString() : "");
            var gv = new GridView();
            gv.DataSource = table;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=excelExport.xls");
            Response.ContentType = "application/ms-excel; charset=utf-8";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            var emp = context.Employees.Where(x => x.Id.ToString() == Id).First();
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            objHtmlTextWriter.Write("<h4>" + emp.FullName + "</h4>");
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
        }
        public ActionResult Save(string Id, string firstDate, string secondDate, string totalSum, string totalBenefit)
        {
            var list = GetList(Id, firstDate, secondDate);
            context.DailyPayouts.Add(new Daily() { EmployeeId = int.Parse(Id), Date = DateTime.Now, Amount = decimal.Parse(totalSum), AdditionalBenefits = totalBenefit!=""?decimal.Parse(totalBenefit):0 });
            context.SaveChanges();
            int rid = context.DailyPayouts.OrderByDescending(p => p.Id).FirstOrDefault().Id;
            return RedirectToAction("Index", "Daily", new { id = rid });
            
        }
        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }
        private List<CalculationModel> GetList(string Id, string firstDate, string secondDate)
        {
            var emp = context.Employees.Where(x => x.Id.ToString() == Id).First();
            ViewBag.CurrencyName = emp.GetPosition().GetCurrency().Name;
            ViewBag.EmpName = emp.FullName;
            ViewBag.DocumentNumber = emp.DocumnentNumber;
            List<CalculationModel> list = new List<CalculationModel>();
            foreach (var item in DateRange(DateTime.Parse(firstDate), DateTime.Parse(secondDate)))
            {
                var currencyid = emp.GetPosition().CurrencyId;
                var date = item.ToShortDateString();
                try
                {
                    var rate = context.ExchangeRates.Where(x => x.CurrencyId == currencyid && x.Date == item).First().Rate;
                    var salary = emp.GetPosition().Salary;
                    var sum = rate * salary;
                    var euroRate = context.ExchangeRates.Where(x => x.CurrencyId == 2 && x.Date == item).First().Rate;
                    decimal euroSalary = 80;
                    var euroSum = euroRate * euroSalary;
                    var addben = (sum - euroSum) > 0 ? Math.Round((sum - euroSum),2).ToString() : "";
                    list.Add(new CalculationModel() { Date = date, Rate = rate.ToString(), Salary = salary.ToString(), Sum = sum.ToString(), EuroRate=euroRate.ToString(), EuroSalary=euroSalary.ToString(), EuroSum = euroSum.ToString(), AdditionalBenefits= addben });
                }
                catch
                {

                }
               
            }
            return list;
        }
        private IEnumerable<DateTime> DateRange(DateTime fromDate, DateTime toDate)
        {
            return Enumerable.Range(0, toDate.Subtract(fromDate).Days + 1)
                             .Select(d => fromDate.AddDays(d));
        }
    }
}