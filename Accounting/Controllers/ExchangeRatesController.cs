﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Accounting.Models;

namespace Accounting.Controllers
{
    public class ExchangeRatesController : Controller
    {
        private AccountingContext db = new AccountingContext();

        public ActionResult Index()
        {
            if (MvcApplication.IsAuthorized())
            {
                var list = db.Currencies.ToList();
                list.Insert(0, new Currency() { Name = "Всі валюти", Id = -5 });
                ViewBag.Currencies = list;
                return View(db.ExchangeRates.ToList());
            }
            else return RedirectToAction("Index", "Auth");
        }
        [HttpPost]
        public ActionResult Index(int currencyId)
        {
            var list = db.Currencies.ToList();
            list.Insert(0, new Currency() { Name = "Всі валюти", Id = -5 });
            ViewBag.Currencies = list;
            if (currencyId != -5)
            {
                return View(db.ExchangeRates.ToList().Where(x => x.CurrencyId == currencyId));
            }
            else return View(db.ExchangeRates);
        }

        public ActionResult Create()
        {
            ViewBag.Currencies = db.Currencies.ToList();
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CurrencyId,Rate,Date")] ExchangeRate exchangeRate)
        {
            if (ModelState.IsValid)
            {
                db.ExchangeRates.Add(exchangeRate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Currencies = db.Currencies.ToList();

            return View(exchangeRate);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExchangeRate exchangeRate = db.ExchangeRates.Find(id);
            if (exchangeRate == null)
            {
                return HttpNotFound();
            }
            ViewBag.Currencies = db.Currencies.ToList();
            return View(exchangeRate);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CurrencyId,Rate,Date")] ExchangeRate exchangeRate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(exchangeRate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Currencies = db.Currencies.ToList();

            return View(exchangeRate);
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
