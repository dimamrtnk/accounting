﻿using Accounting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Accounting.Controllers
{
    public class DailyController : Controller
    {
        private AccountingContext db = new AccountingContext();

        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Daily daily = db.DailyPayouts.Find(id);
            if (daily == null)
            {
                return HttpNotFound();
            }
            return View(daily);
        }
        [HttpGet]
        public ActionResult Data()
        {
            var list = db.Employees.ToList();
            list.Insert(0, new Employee() { LastName = "Всі", FirstName = "співробітники", Id = -5 });
            ViewBag.Employees = list;
            return View(db.DailyPayouts);
        }
        [HttpPost]
        public ActionResult Data(int employeeId)
        {
            var list = db.Employees.ToList();
            list.Insert(0, new Employee() { LastName = "Всі", FirstName = "співробітники", Id = -5 });
            ViewBag.Employees = list;

            if (employeeId == -5)
                return View(db.DailyPayouts);
            else return View(db.DailyPayouts.Where(x => x.EmployeeId == employeeId));
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}