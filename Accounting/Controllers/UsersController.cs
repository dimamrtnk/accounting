﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Accounting.Models;

namespace Accounting.Controllers
{
    public class UsersController : Controller
    {
        private AccountingContext db = new AccountingContext();

        public ActionResult Index()
        {
            if (MvcApplication.IsAuthorized())
            {
                if (MvcApplication.GetUser().AccessUsers == true)
                {
                    return View(db.Users.ToList());
                }
                else
                {
                    return new HttpUnauthorizedResult();
                }
            }
            else
            {
                return RedirectToAction("Index", "Auth");
            }
        }

       
        public ActionResult Create()
        {
            if (MvcApplication.IsAuthorized())
            {
                if (MvcApplication.GetUser().AccessUsers == true)
                {
                    return View();
                }
                else
                {
                    return new HttpUnauthorizedResult();
                }
            }
            else
            {
                return RedirectToAction("Index", "Auth");
            }
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Login,Password,AccessCalc,AccessCurrencies,AccessEmployees,AccessPayouts,AccessPositions,AccessRates,AccessUsers")] User user)
        {
            if (MvcApplication.IsAuthorized())
            {
                if (MvcApplication.GetUser().AccessUsers == true)
                {
                    if (ModelState.IsValid)
                    {
                        db.Users.Add(user);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    return new HttpUnauthorizedResult();
                }
            }
            else
            {
                return RedirectToAction("Index", "Auth");
            }
            return View(user);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            if (MvcApplication.IsAuthorized())
            {
                if (MvcApplication.GetUser().AccessUsers == true)
                {
                    return View(user);
                }
                else
                {
                    return new HttpUnauthorizedResult();
                }
            }
            else
            {
                return RedirectToAction("Index", "Auth");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Login,Password,AccessCalc,AccessCurrencies,AccessEmployees,AccessPayouts,AccessPositions,AccessRates,AccessUsers")] User user)
        {
            if (MvcApplication.IsAuthorized())
            {
                if (MvcApplication.GetUser().AccessUsers == true)
                {
                    if (ModelState.IsValid)
                    {
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    return new HttpUnauthorizedResult();
                }
            }
            else
            {
                return RedirectToAction("Index", "Auth");
            }
            return View(user);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            if (MvcApplication.IsAuthorized())
            {
                if (MvcApplication.GetUser().AccessUsers == true)
                {
                    return View(user);
                }
                else
                {
                    return new HttpUnauthorizedResult();
                }
            }
            else
            {
                return RedirectToAction("Index", "Auth");
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (MvcApplication.IsAuthorized())
            {
                if (MvcApplication.GetUser().AccessUsers == true)
                {
                    User user = db.Users.Find(id);
                    db.Users.Remove(user);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return new HttpUnauthorizedResult();
                }
            }
            else
            {
                return RedirectToAction("Index", "Auth");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
