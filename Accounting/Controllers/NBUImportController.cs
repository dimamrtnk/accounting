﻿using Accounting.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Accounting.Controllers
{
    public class NBUImportController : Controller
    {
        AccountingContext context = new AccountingContext();
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Import(string firstDate, string secondDate)
        {
            foreach (var item in context.Currencies)
            {
                foreach (var date in DateRange(DateTime.Parse(firstDate), DateTime.Parse(secondDate)))
                {
                    try
                    {
                        HttpClient client = new HttpClient();

                        string uri = String.Format("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode={0}&date={1}", item.Name, date.ToString("yyyyMMdd"));

                        string response = await client.GetStringAsync(uri);

                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(response);
                        XmlElement root = doc.DocumentElement;

                        string rate = root.FirstChild["rate"].InnerText;
                        var numberFormatInfo = new NumberFormatInfo { NumberDecimalSeparator = "." };
                        decimal d = decimal.Parse(rate, numberFormatInfo);

                        context.ExchangeRates.Add(new ExchangeRate() { CurrencyId = item.Id, Date = date, Rate = d });
                    }
                    catch
                    {

                    }
                   
                }
            }
            await context.SaveChangesAsync();

            return RedirectToAction("Index", "ExchangeRates");
        }

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }
        private IEnumerable<DateTime> DateRange(DateTime fromDate, DateTime toDate)
        {
            return Enumerable.Range(0, toDate.Subtract(fromDate).Days + 1)
                             .Select(d => fromDate.AddDays(d));
        }
    }
}