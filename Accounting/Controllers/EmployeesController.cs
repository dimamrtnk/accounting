﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Accounting.Models;

namespace Accounting.Controllers
{
    public class EmployeesController : Controller
    {
        private AccountingContext db = new AccountingContext();

        public ActionResult Index()
        {
            if (MvcApplication.IsAuthorized())
                return View(db.Employees);
            else return RedirectToAction("Index","Auth");
        }

      

        public ActionResult Create()
        {
            ViewBag.Positions = db.Positions.ToArray();
            return View();
        }

      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,SurName,LastName,Gender,Document,DocumentSeries,DocumnentNumber,ITN,PositionID")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Positions = db.Positions.ToArray();

            return View(employee);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.Positions = db.Positions.ToArray();

            return View(employee);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,SurName,LastName,Gender,Document,DocumentSeries,DocumnentNumber,ITN,PositionID")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Positions = db.Positions.ToArray();
            return View(employee);
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
