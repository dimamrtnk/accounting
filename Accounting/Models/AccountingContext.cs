﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Accounting.Models
{
    public class AccountingContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<ExchangeRate> ExchangeRates { get; set; }
        public DbSet<Daily> DailyPayouts { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExchangeRate>()
                        .Property(p => p.Rate)
                        .HasPrecision(18, 6); // or whatever your schema specifies
        }
    }
    public class AccountingDbInitializer : DropCreateDatabaseAlways<AccountingContext>
    {
        protected override void Seed(AccountingContext context)
        {
            //
            context.Users.Add(new User(true) { Login = "Admin", Password = "1111"});
            context.Currencies.Add(new Currency() { Name = "USD" });
            context.Currencies.Add(new Currency() { Name = "EUR" });
            //context.Positions.Add(new Position() { Name = "Менеджер", Salary = 300, CurrencyId = context.Currencies.Where(x => x.Name == "USD").First().Id });
            //context.Positions.Add(new Position() { Name = "Касир", Salary = 50, CurrencyId = context.Currencies.Where(x => x.Name == "USD").First().Id });
            //context.Positions.Add(new Position() { Name = "Охоронець", Salary = 80, CurrencyId = context.Currencies.Where(x => x.Name == "USD").First().Id });
            //context.Employees.Add(new Employee() { FirstName = "Іван", SurName = "Іванович", LastName = "Іванов", Gender = "чол", Document = "Паспорт",
            //    DocumentSeries = "МТ", DocumnentNumber = "1234567", ITN = "4343255433232",
            //    PositionID = context.Positions.Where(x => x.Name == "Охоронець").First().Id });

            base.Seed(context);
        }
    }
}