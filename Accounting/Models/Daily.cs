﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Accounting.Models
{
    public class Daily
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        [Display(Name = "Дата")]
        public DateTime Date { get; set; }
        [Display(Name = "Всього нараховано за відрядження, грн")]
        public decimal Amount { get; set; }
        [Display(Name = "Додаткове благо")]
        public decimal AdditionalBenefits { get; set; }
        [Display(Name = "ПІБ Співробітника")]
        public string EmployeeName
        {
            get
            {
                using (AccountingContext c = new AccountingContext())
                {
                    return c.Employees.Where(x => x.Id == EmployeeId).First().FullName;
                }
            }
        }
    }
}