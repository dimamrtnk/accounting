﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Accounting.Models
{
    public class Position
    {
        public int Id { get; set; }
        [Display(Name = "Назва")]
        public string Name { get; set; }
        [Display(Name = "Оклад")]
        public decimal Salary { get; set; }
        public int CurrencyId { get; set; }

        [Display(Name = "Валюта")]
        public string CurrencyName => GetCurrency().Name;
        
        public Currency GetCurrency()
        {
            using (var c = new AccountingContext())
            {

                return c.Currencies.Where(x => x.Id == CurrencyId).First();

            }
        }
    }
}