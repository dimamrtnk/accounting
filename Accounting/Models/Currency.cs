﻿using System.ComponentModel.DataAnnotations;

namespace Accounting.Models
{
    public class Currency
    {
        public int Id { get; set; }
        [Display(Name = "Назва")]
        public string Name { get; set; }

        
    }
}