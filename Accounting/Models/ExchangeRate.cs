﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Accounting.Models
{
    public class ExchangeRate
    {
        public int Id { get; set; }
        public int CurrencyId { get; set; }
        [DisplayFormat(DataFormatString = "{0:00.000000}")]
        public decimal Rate { get; set; }
        public DateTime Date { get; set; }

        public string CurrencyName
        {
            get
            {
                using (var c = new AccountingContext())
                {
                    return c.Currencies.Where(x => x.Id == CurrencyId).First().Name;
                }
            }
        }
    }
}