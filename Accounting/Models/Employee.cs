﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Accounting.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Document { get; set; }
        public string DocumentSeries { get; set; }
        public string DocumnentNumber { get; set; }
        //  [StringLength(10, MinimumLength = 10, ErrorMessage = "ІПН має складатись з 10 цифр!")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "ІПН має складатись з 10 цифр!")]
        public string ITN { get; set; }
        public int PositionID { get; set; }

        public string FullName => LastName + " " + FirstName + " " + SurName;

        public Position GetPosition()
        {
            using (var c = new AccountingContext())
            {

                return c.Positions.Where(x => x.Id == PositionID).First();

            }
        }

        public string PositionName
        {
            get
            {
                using (var c = new AccountingContext())
                {
                    try
                    {
                        return c.Positions.Where(x => x.Id == PositionID).First().Name;
                    }
                    catch
                    {
                        return "";

                    }
                }
            }
        }
    }
}