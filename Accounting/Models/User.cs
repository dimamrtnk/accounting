﻿using System.ComponentModel.DataAnnotations;

namespace Accounting.Models
{
    public class User
    {
        public int Id { get; set; }
        [Display(Name = "Логін")]
        public string Login { get; set; }
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        [Display(Name = "Кадри")]
        public bool AccessEmployees { get; set; }
        [Display(Name = "Розрахунок добових")]
        public bool AccessCalc { get; set; }
        [Display(Name = "Історія нарахувань")]
        public bool AccessPayouts { get; set; }
        [Display(Name = "Курси валют")]
        public bool AccessRates { get; set; }
        [Display(Name = "Посади")]
        public bool AccessPositions { get; set; }
        [Display(Name = "Валюти")]
        public bool AccessCurrencies { get; set; }
        [Display(Name = "Користувачі")]
        public bool AccessUsers { get; set; }

        public User()
        {

        }
        public User(bool grantAllAccess)
        {
            if(grantAllAccess)
            {
                AccessCalc = true;
                AccessCurrencies = true;
                AccessEmployees = true;
                AccessPayouts = true;
                AccessPositions = true;
                AccessRates = true;
                AccessUsers = true;
            }
        }
    }
}