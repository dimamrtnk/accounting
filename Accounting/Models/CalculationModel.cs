﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Accounting.Models
{
    public class CalculationModel
    {
        public string Date { get; set; }
        public string Rate { get; set; }
        public string Salary { get; set; }
        public string Sum { get; set; }
        public string EuroRate { get; set; }
        public string EuroSalary { get; set; }
        public string EuroSum { get; set; }
        public string AdditionalBenefits { get; set; }
    }
}