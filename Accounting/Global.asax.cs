﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Accounting.Models;
namespace Accounting
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
           // Database.SetInitializer(new AccountingDbInitializer());
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public static bool IsAuthorized()
        {
            var cookie = HttpContext.Current.Request.Cookies["isauth"];
            if (cookie?.Value == "1")
                return true;
            else return false;
        }

        public static User GetUser()
        {
            if (IsAuthorized())
            {
                using (AccountingContext c = new AccountingContext())
                {
                    string username = HttpContext.Current.Request.Cookies["user-login"].Value;
                    return c.Users.Where(x => x.Login == username).First();
                }
            }
            else return null;
        }
    }
}
